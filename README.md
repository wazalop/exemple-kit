# explications

At work, we are looking into migrating from django template to a js framework.
Since I like svelte, I'm looking into a solution that would use svelte-kit.(but I think the majority of issue here would be present in the other meta framework)

The app is quite complex, so we need to be able to migrate only part of it at a time. (and when all or the majority of the pages are done, completely separate front from back, still in discussion ^^)

the django folder is a simplified example of a django app

- the django app will render the templates base on the django urls (django code will call render(template)), we will make sure the svelte-kit url correspond to the django one and use pre-rendered pages as template
- then django app will serve static file base on the path : `/static/frontendapp/[files]`


# problemes

- is there a way to tell svelte-kit all my assets need to be prefixed with `/static/frontendapp/` ?

in the code it assumes assets are on the root of the server. (when [#9341](https://github.com/sveltejs/kit/issues/9341) will be merge)

using `path.base` block the build because the urls are base on the server root (`/about` for exemple) and svelkit want us to prefix all url with `path.base`

using `path.assets` don't take server root path, our app is serve from multiple subdomain :/ . (see [#3998](https://github.com/sveltejs/kit/issues/3998))

- is there a way to tell svelte-kit to ignore 404 on links when building?

for now if a link is not found on build, the build stop. Look in +layout.svelte, there is a link to a django page that is not in the svelte-kit app,
on build svelte-kit will stop on this link complaining the page doesn't exist (we could create empty page in the meantime, but it's bot ideal)